
import org.apache.hadoop.fs.FileSystem;

import java.io.IOException; 
import java.util.StringTokenizer; 
import java.io.*;

import org.apache.hadoop.conf.Configuration; 
import org.apache.hadoop.fs.Path; 
import org.apache.hadoop.io.IntWritable; 
import org.apache.hadoop.io.Text; 
import org.apache.hadoop.mapreduce.Job; 
import org.apache.hadoop.mapreduce.Mapper; 
import org.apache.hadoop.mapreduce.Reducer; 
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat; 
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat; 

enum Counter {Malfor};

public class LongestWord
 { 
 	static int malcounter =0;

    public static class TokenizerMapper 
            extends Mapper<Object, Text, Text, IntWritable>
	{ 
		private static final String delimiters = " !\"#$%&()*+,-./:;<=>@[\\]^_~\n\t\r";
	
        private static IntWritable one;
        private Text word = new Text(); 
 
		static boolean isGood(String str)
		{
			byte[] arr = str.getBytes();
			for(int i=0; i<str.length();i++)
			{
			if ((arr[i]<64 )  ||  ((arr[i]>91) && (arr[i]<97))  ||   (arr[i]>122))
				{
					return false;
				}
			}
			return true;
			
		}
		
        public void map(Object key, Text value, Context context  ) throws IOException, InterruptedException 
		{ 
            StringTokenizer itr = new StringTokenizer(value.toString(),delimiters ); 
            while (itr.hasMoreTokens())
			{ 
				String stri = itr.nextToken();
				if (isGood(stri))
				{
					one= new IntWritable(stri.length()); 
					word.set(stri); 
					context.write(word, one);
				}			
				else
				{
					++malcounter;
					 context.getCounter(Counter.Malfor).increment(1);
				}
				
            } 
        } 
		
    } 
 
    public static class IntSumReducer 
            extends Reducer<Text,IntWritable,Text,IntWritable>
	{ 
        private IntWritable result = new IntWritable(); 
		 static int maxi=0;
		private static Text keymax=new Text();

		
 
        public void reduce(Text key, Iterable<IntWritable> values, Context context ) throws IOException, InterruptedException 
		{ 
            for (IntWritable val : values) 
			{ 
				if ( val.get()> maxi)
				{
					maxi=val.get();
					keymax.set(key);
				}				
            } 
        } 
		
		 public void cleanup(Context context) throws IOException, InterruptedException
        {
			Text textic=new Text("\nmalformed: ");
            result.set(maxi);
			context.write(keymax, result); 
        }
    } 
	
	
	
 	public static void replaceLines() {
    try {
        // input the (modified) file content to the StringBuffer "input"
		
				Configuration conf = new Configuration();
				conf.set("fs.defaultFS", "hdfs://172.18.0.2:9000");
				FileSystem fs = FileSystem.get(conf);

					fs.copyToLocalFile(new Path("out/part-r-00000"), new Path("outlock/output"));
 	
		
        BufferedReader file = new BufferedReader(new FileReader("outlock/output"));
        StringBuffer inputBuffer = new StringBuffer();
        String line;

        while ((line = file.readLine()) != null) {
            line.replace('\t', ',');
			line.replace(' ', ',');
            inputBuffer.append(line);
            inputBuffer.append('\n');
        }
        file.close();

        // write the new string with the replaced line OVER the same file
        FileOutputStream fileOut = new FileOutputStream("outlock/output2");
        fileOut.write(inputBuffer.toString().getBytes());
        fileOut.close();

    } catch (Exception e) {
        System.out.println("Problem with reading file. "+e.getMessage());
    }
}
 
    public static void main(String[] args) throws Exception 
	{ 
        Configuration conf = new Configuration(); 
        Job job = Job.getInstance(conf, "word count"); 
        job.setJarByClass(LongestWord.class); 
        job.setMapperClass(TokenizerMapper.class); 
        job.setReducerClass(IntSumReducer.class); 
        job.setOutputKeyClass(Text.class); 
        job.setOutputValueClass(IntWritable.class); 
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
              if(job.waitForCompletion(true))
        {
			
            System.out.println("\nok\n" );
			System.out.println("malformed: " + job.getCounters().findCounter(Counter.Malfor).getValue());
            replaceLines();
			System.exit(0);
        }
        System.exit(1);
    } 
}
